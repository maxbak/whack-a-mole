module count_incrementer(
	input          clock_i,
	input            hit_i,
	output reg increment_o
);
	reg clock_toggle;
	reg button_toggle;
	reg hit;

	// for simulation purposes
	initial
		button_toggle = 0;

	always @(posedge clock_i)
		clock_toggle = ~button_toggle;

	always @(posedge hit_i)
		button_toggle = clock_toggle;

	always @(*)
		increment_o = ~( clock_toggle ^ button_toggle );
endmodule
