`include "bin_2_one_hot.v"
`include "clock_divider.v"
`include "rand.v"
`include "count_incrementer_top.v"
`include "display_control.v"

module whack_a_mole (
	input              clock_i,
	input              reset_i,
	input  [1:0]       level_i,
	input  [4:0]     buttons_i,
	output [4:0]       moles_o,
	output [6:0]        disp_o,
	output [7:0] disp_select_o
);
	
	wire clock_1k;
	wire clock_4s;
	wire clock_2s;
	wire clock_1s;
	wire clock_500ms;
	wire game_enable;
	wire score_add;

	wire [2:0] rand_num;

	reg game_clock;
	reg start_i;

	// used to imitate press any button to begin
	always @(*) begin
		start_i = |buttons_i;
	end
	
	defparam clk_1k.DIVISOR    = 100 * 10**3;
	defparam clk_4s.DIVISOR    = 100 * 10**6 / 4;
	defparam clk_2s.DIVISOR    = 100 * 10**6 / 2;
	defparam clk_1s.DIVISOR    = 100 * 10**6;
	defparam clk_500ms.DIVISOR = 100 * 10**6 * 2;

	clock_divider clk_1k (
		.clock_i ( clock_i  ),
		.clock_o ( clock_1k )
	);

	clock_divider clk_4s (
		.clock_i ( clock_i  ),
		.clock_o ( clock_4s )
	);

	clock_divider clk_2s ( 
		.clock_i ( clock_i  ),
		.clock_o ( clock_2s )
	);

	clock_divider clk_1s ( 
		.clock_i ( clock_i  ),
		.clock_o ( clock_1s )
	);

	clock_divider clk_500ms ( 
		.clock_i ( clock_i     ),
		.clock_o ( clock_500ms )
	);

	always @(*) begin
		case(level_i)
			2'd0 : game_clock = clock_4s    & game_enable;
			2'd1 : game_clock = clock_2s    & game_enable;
			2'd2 : game_clock = clock_1s    & game_enable;
			2'd3 : game_clock = clock_500ms & game_enable;
		endcase
	end

	rand num_gen (
		.init_i ( game_clock ),
		 .num_o ( rand_num   )
	);

	bin_2_one_hot one_hot (
		   .bin_i (  rand_num    ),
		.enable_i (  game_enable ),
		   .hot_o ( moles_o      )
	);

	count_incrementer_top add_score (
		    .clock_i (   clock_i   ),
		    .moles_i (   moles_o   ),
		  .buttons_i ( buttons_i   ),
		.increment_o (   score_add )
	);

	display_control disp (
		       .clock_i (       clock_1k ),
		       .score_i ( /* needs to be added */ ),
                        .time_i ( /* needs to be added */ ),
		        .disp_o (        disp_o  ),
		.digit_select_o ( disp_select_o  )
	);

endmodule
